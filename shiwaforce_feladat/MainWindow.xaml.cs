﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace shiwaforce_feladat
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {

        Szamolo szamolo;
        public MainWindow()
        {
            InitializeComponent();
            szamolo = new Szamolo();
            DataContext = szamolo;
            lb_visszajaro.ItemsSource = szamolo.Kiiro;
            
        }

        
        private void tb_fizetendo_TextChanged(object sender, TextChangedEventArgs e)
        {
            this.szamolo.Szamol(tb_fizetendo.Text,tb_fizetett.Text);   
        }

        private void tb_fizetendo_PreviewTextInput(object sender, TextCompositionEventArgs e)
        {
            Regex regex = new Regex("[^0-9]+");
            e.Handled = regex.IsMatch(e.Text);
        }
    }
}
