﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;
using System.Threading.Tasks;

namespace shiwaforce_feladat
{
    enum penztype { huszezres = 20000, tizezres = 10000, otezres = 5000, ketezres = 2000, ezres = 1000, otszazas = 500, ketszazas = 200, szazas = 100, otvenes = 50, huszas = 20, tizes = 10, otos = 5}
    public class Szamolo : INotifyPropertyChanged
    {
        ObservableCollection<int> visszajaro;
        ObservableCollection<string> kiiro;
        int fizetendo;
        int fizetett;
        string hiba;

        public event PropertyChangedEventHandler PropertyChanged;

        public ObservableCollection<int> Visszajaro
        {
            get
            {
                return visszajaro;
            }

            set
            {
                visszajaro = value;
                //OnPropertyChanged();  
            }
        }

        public int Fizetendo
        {
            get
            {
                return fizetendo;
            }

            set
            {
                fizetendo = value;
            }
        }

        public int Fizetett
        {
            get
            {
                return fizetett;
            }

            set
            {
                fizetett = value;
            }
        }

        public ObservableCollection<string> Kiiro
        {
            get
            {
                return kiiro;
            }

            set
            {
                kiiro = value;
            }
        }

        public string Hiba
        {
            get
            {
                return hiba;
            }

            set
            {
                hiba = value;
                OnPropertyChanged();
            }
        }

        public Szamolo()
        {
            this.visszajaro = new ObservableCollection<int>();
            this.Kiiro = new ObservableCollection<string>();
            this.Fizetendo = 0;
            this.Fizetett = 0;
            for (int i = 0; i < 12; i++)
            {
                visszajaro.Add(0);
            }
            
        }

        private void OnPropertyChanged
            ([CallerMemberName] string propertyName = "")
        {
            PropertyChangedEventHandler handler
                = PropertyChanged;
            if (handler != null)
            {
                handler
                    (this, new PropertyChangedEventArgs(propertyName));
            }

        }
        public void Szamol(string tb_fizetendo, string tb_fizetett)
        {
            Alaphelyzetbe();
               
            if (tb_fizetendo == "")
            {
                Fizetendo = 0;
            }
            else
            {
                Fizetendo = int.Parse(tb_fizetendo);
            }
            if (tb_fizetett == "")
            {
                Fizetett = 0;
            }
            else
            {
                Fizetett = int.Parse(tb_fizetett);
            }

            if (Fizetendo > Fizetett)
            {
                Hiba = "Kevés pénzt adott!";
            }
            else
            {
                Hiba = "";
                int ossz_visszajaro = Visszajaro_Kiszamolas(Fizetendo, Fizetett);

                var value = Enum.GetValues(typeof(penztype));

                
                
                int visszajaro_index = 0;
                for (int i = value.Length -1 ; i >= 0; i--)
                {
                    while (ossz_visszajaro != 0 && ossz_visszajaro >= (int)value.GetValue(i))
                    {
                        ossz_visszajaro -= (int)value.GetValue(i);
                        Visszajaro[visszajaro_index]++;
                    }
                    visszajaro_index++;
                }
                Kiiras();
            }
        }
        void Kiiras()
        {
            var value = Enum.GetValues(typeof(penztype));
            
            for (int i = value.Length - 1; i >= 0; i--)
            {
                if (Visszajaro[(Visszajaro.Count - 1) - i] != 0)
                {
                    Kiiro.Add(String.Format("{0} db {1} bankjegy", Visszajaro[(Visszajaro.Count - 1) - i], value.GetValue(i)));
                }
            }
            ;
        }
        void Alaphelyzetbe()
        {
            for (int i = 0; i < 12; i++)
            {
                visszajaro[i] = 0;
            }

            Kiiro.Clear();
        }
        int Visszajaro_Kiszamolas(int fizetendo, int fizetett)
        {
            int kerekitetlen_visszajaro = fizetett - fizetendo;
            int kerekites = kerekitetlen_visszajaro % 5;
            if (kerekites >= 3)
            {
                kerekitetlen_visszajaro += (5 - kerekites);
            }
            else
            {
                kerekitetlen_visszajaro -= kerekites;
            }

            return kerekitetlen_visszajaro;
        }
    }
}
